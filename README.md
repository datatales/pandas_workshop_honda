# pandas_workshop_honda

This workshop addresses the core components of ```pandas``` and shows examples on how to use them for small and larger projects.

## Getting started
Please clone this repo by 

```
cd your_folder
git clone https://gitlab.com/datatales/pandas_workshop_honda.git
```
install the required packages to your environment with ```pip``` or ```conda```
```
pip install -r <path/to/requirement.txt>
```

and update code base later:
```
cd your_folder
git pull origin main
```

## Topics
* Loading and customizing data sets from different sources.  
* Data structures: Array, List, Series and DataFrame  
* Basic insights and manipulations of datasets:  
  * Simple Statistics  
  * Querying and Sorting  
  * Grouping and Aggregating  
  * Data Cleaning, Invalid Values  
* Time optimization data processing  
* Views vs. Copies  
* Plotting with Matplotlib  
  * Diagram types  
  * Styling  
* Time Series
* Real life examples and projects

## Recommended Ressources

- [ ] [Google Colab](https://colab.research.google.com/drive/11wvKC01nDQlRg0i3Z46yRNvkkrrORrKi?usp=sharing)
pip - [ ] https://pandas.pydata.org/docs/index.html
- [ ] https://jakevdp.github.io/PythonDataScienceHandbook/
- [ ] https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf
